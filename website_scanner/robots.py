from urllib import urlopen
from contextlib import closing

def get_robots_txt(url):
  if url.endswith('/'):
    path = url
  else:
    path = url + '/'
  
  url = path + "robots.txt"

  with closing(urlopen(url)) as stream:
      return stream.read()
