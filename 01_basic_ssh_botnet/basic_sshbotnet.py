import paramiko

class Client:
  def __init__(self,host,port,user,password):
    self.host = {'host': host,'port': port, 'user': user, 'password': password}
    self.session = self.connection()

  def connection(self):
    try:
      client = paramiko.SSHClient()
      client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
      client.connect(self.host['host'],self.host['port'],self.host['user'],self.host['password'])
      return client
    except Exception as e:
      print(e)
      print("[-] Error Connecting")

  def send_command(self,cmd):
    print("[*] Output from " + self.host['host'])
    print("[*] =========================")
    stdin, stdout, stderr = self.session.exec_command(cmd)
    for line in stdout:
      print("[*] " + line.strip('\n'))
    print("[*]")
    self.session.close()


def add_client(host,port,user,password):
  client = Client(host,port,user,password)
  botNet.append(client)

def botnetCommand(cmd):
  for client in botNet:
    client.send_command(cmd)

botNet = []
add_client('192.168.1.179',22,'vagrant',"password")
botnetCommand("ls")