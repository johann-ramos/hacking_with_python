# To Install BeautifulSoup
# pip didn't work for me
# sudo apt-get install -y python3-bs4
# sudo dnf install -y python3-beautifulsoup4

import argparse
import os
import time
import urllib
from urllib.parse import urlparse
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

def getPeopleLinks(page):
  links = []
  for link in page.find_all('a'):
    url = link.get('href')
    if url:
      if 'profile/view?id=' in url:
        links.append(url)
  return links

def getJobsLinks(page):
  links = []
  for link in page.find_all('a'):
    url = link.get('href')
    if url:
      if '/jobs' in url:
        links.append(url)
  return links

def getId(url):
  p_url = urlparse(url)
  return urllib.parse.parse_qs(p_url.query)['id'][0]

def viewBot(browser):
  visited = {}
  p_list = []
  count = 0
  while True:
    time.sleep(random.uniform(3.5,6.9))
    page = BeautifulSoup(browser.page_source)
    people = getPeopleLinks(page)
    if people:
      for person in people:
        pid = getId(person)
        if pid not in visited:
          p_list.append(person)
          visited[pid] = 1

    if p_list:
      person = p_list.pop()
      browser.get(person)
      count += 1
    else:
      jobs = getJobsLinks(page)
      if jobs:
        job = random.choice(jobs)
        root = 'http://www.linkedin.com'
        roots = 'http://www.linkedin.com'
        if root not in job or roots not in job:
          job = 'https://linkedin.com'+job
        browser.get(job)
      else:
        print("Im lost, Exiting")
        break
    
    print("[+] "+browser.title+" Visited!\n["+str(count)+"/"+str(len(p_list))+"] Visited/Queue")  

def Main():
  parser = argparse.ArgumentParser()
  parser.add_argument("email", help="linkedin email")
  parser.add_argument("password", help="linkedin password")
  args = parser.parse_args()

  browser = webdriver.Firefox()
  browser.get("https://linkedin.com/uas/login")
  emailElement = browser.find_element_by_id("session_key-login")
  emailElement.send_keys(args.email)
  passElement = browser.find_element_by_id("session_password-login")
  passElement.send_keys(args.password)
  passElement.submit()

  os.system('clear')
  print("[+] Success Logged In, Bot Starting")
  viewBot(browser)
  browser.close()

if __name__ == "__main__":
  Main()
